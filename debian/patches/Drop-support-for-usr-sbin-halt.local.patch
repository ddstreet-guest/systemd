From: Michael Biebl <biebl@debian.org>
Date: Tue, 14 May 2019 17:40:45 +0200
Subject: Drop support for /usr/sbin/halt.local

/usr/sbin/halt.local is a Fedora/Red Hat anachronism from pre-systemd
times.

(cherry picked from commit 44508946534eee032927c263b79464832656dd6e)
---
 docs/DISTRO_PORTING.md                      |  1 -
 man/systemd-rc-local-generator.xml          | 12 ++++--------
 meson.build                                 |  3 ---
 meson_options.txt                           |  2 --
 src/rc-local-generator/rc-local-generator.c |  6 ------
 units/meson.build                           |  1 -
 6 files changed, 4 insertions(+), 21 deletions(-)

diff --git a/docs/DISTRO_PORTING.md b/docs/DISTRO_PORTING.md
index 620e47e..d14bf13 100644
--- a/docs/DISTRO_PORTING.md
+++ b/docs/DISTRO_PORTING.md
@@ -15,7 +15,6 @@ distribution:
    * `-Dsysvinit-path=`
    * `-Dsysvrcnd-path=`
    * `-Drc-local=`
-   * `-Dhalt-local=`
    * `-Dloadkeys-path=`
    * `-Dsetfont-path=`
    * `-Dtty-gid=`
diff --git a/man/systemd-rc-local-generator.xml b/man/systemd-rc-local-generator.xml
index 514d102..81744c2 100644
--- a/man/systemd-rc-local-generator.xml
+++ b/man/systemd-rc-local-generator.xml
@@ -17,7 +17,7 @@
 
   <refnamediv>
     <refname>systemd-rc-local-generator</refname>
-    <refpurpose>Compatibility generator for starting <filename>/etc/rc.local</filename> and <filename>/usr/sbin/halt.local</filename> during boot and shutdown</refpurpose>
+    <refpurpose>Compatibility generator for starting <filename>/etc/rc.local</filename> during boot</refpurpose>
   </refnamediv>
 
   <refsynopsisdiv>
@@ -35,14 +35,10 @@
     script is run after <filename>network.target</filename>, but in parallel with most other regular system
     services.</para>
 
-    <para><filename>systemd-rc-local-generator</filename> also checks whether <filename>/usr/sbin/halt.local</filename>
-    exists and is executable, and if it is pulls the <filename>halt-local.service</filename> unit into the shutdown
-    process. This unit is responsible for running this script during later shutdown.</para>
-
-    <para>Support for both <filename>/etc/rc.local</filename> and <filename>/usr/sbin/halt.local</filename> is provided
+    <para>Support for <filename>/etc/rc.local</filename> is provided
     for compatibility with specific System V systems only. However, it is strongly recommended to avoid making use of
-    these scripts today, and instead provide proper unit files with appropriate dependencies for any scripts to run
-    during the boot or shutdown processes.</para>
+    this script today, and instead provide proper unit files with appropriate dependencies for any scripts to run
+    during the boot process.</para>
 
     <para><filename>systemd-rc-local-generator</filename> implements
     <citerefentry><refentrytitle>systemd.generator</refentrytitle><manvolnum>7</manvolnum></citerefentry>.</para>
diff --git a/meson.build b/meson.build
index 79195c9..71e08d7 100644
--- a/meson.build
+++ b/meson.build
@@ -190,7 +190,6 @@ conf.set_quoted('SYSTEM_DATA_UNIT_PATH',                      systemunitdir)
 conf.set_quoted('SYSTEM_SYSVINIT_PATH',                       sysvinit_path)
 conf.set_quoted('SYSTEM_SYSVRCND_PATH',                       sysvrcnd_path)
 conf.set_quoted('RC_LOCAL_SCRIPT_PATH_START',                 get_option('rc-local'))
-conf.set_quoted('RC_LOCAL_SCRIPT_PATH_STOP',                  get_option('halt-local'))
 
 conf.set('ANSI_OK_COLOR',                                     'ANSI_' + get_option('ok-color').underscorify().to_upper())
 
@@ -277,7 +276,6 @@ substs.set('RANDOM_SEED',                                     join_paths(randoms
 substs.set('SYSTEM_SYSVINIT_PATH',                            sysvinit_path)
 substs.set('SYSTEM_SYSVRCND_PATH',                            sysvrcnd_path)
 substs.set('RC_LOCAL_SCRIPT_PATH_START',                      get_option('rc-local'))
-substs.set('RC_LOCAL_SCRIPT_PATH_STOP',                       get_option('halt-local'))
 substs.set('MEMORY_ACCOUNTING_DEFAULT',                       memory_accounting_default ? 'yes' : 'no')
 substs.set('HIGH_RLIMIT_NOFILE',                              conf.get('HIGH_RLIMIT_NOFILE'))
 
@@ -3112,7 +3110,6 @@ status = [
         'bash completions directory:        @0@'.format(bashcompletiondir),
         'zsh completions directory:         @0@'.format(zshcompletiondir),
         'extra start script:                @0@'.format(get_option('rc-local')),
-        'extra stop script:                 @0@'.format(get_option('halt-local')),
         'debug shell:                       @0@ @ @1@'.format(get_option('debug-shell'),
                                                               get_option('debug-tty')),
         'TTY GID:                           @0@'.format(tty_gid),
diff --git a/meson_options.txt b/meson_options.txt
index d4ec37d..e1f700a 100644
--- a/meson_options.txt
+++ b/meson_options.txt
@@ -31,8 +31,6 @@ option('telinit-path', type : 'string', value : '/lib/sysvinit/telinit',
        description : 'path to telinit')
 option('rc-local', type : 'string',
        value : '/etc/rc.local')
-option('halt-local', type : 'string',
-       value : '/usr/sbin/halt.local')
 
 option('quotaon-path', type : 'string', description : 'path to quotaon')
 option('quotacheck-path', type : 'string', description : 'path to quotacheck')
diff --git a/src/rc-local-generator/rc-local-generator.c b/src/rc-local-generator/rc-local-generator.c
index 7a3948e..908e627 100644
--- a/src/rc-local-generator/rc-local-generator.c
+++ b/src/rc-local-generator/rc-local-generator.c
@@ -65,12 +65,6 @@ static int run(const char *dest, const char *dest_early, const char *dest_late)
                 r = add_symlink("rc-local.service", "multi-user.target");
         }
 
-        if (check_executable(RC_LOCAL_SCRIPT_PATH_STOP) >= 0) {
-                log_debug("Automatically adding halt-local.service.");
-
-                k = add_symlink("halt-local.service", "final.target");
-        }
-
         return r < 0 ? r : k;
 }
 
diff --git a/units/meson.build b/units/meson.build
index a561050..52f8b77 100644
--- a/units/meson.build
+++ b/units/meson.build
@@ -118,7 +118,6 @@ units = [
 in_units = [
         ['debug-shell.service',                  ''],
         ['emergency.service',                    ''],
-        ['halt-local.service',                   'HAVE_SYSV_COMPAT'],
         ['initrd-cleanup.service',               ''],
         ['initrd-parse-etc.service',             ''],
         ['initrd-switch-root.service',           ''],
